This is a CDMS fork of the MIDAS repository.

MIDAS is a modern data acquisition system developed at PSI and TRIUMF.

Documentation can be found at https://midas.triumf.ca/MidasWiki/index.php/Midas_documentation.  You can report bugs directly on the Midas bitbucket page (https://bitbucket.org/tmidas/midas) and open issues on the Midas elog (https://midas.triumf.ca/elog/Midas/).


The primary branches in this repository are `master`, `cdms_dev`, and `midas_dev`.  The intent of the branch management is to provide a reliable branch that allows take-it-for-granted use (`master`), allow local development of the code (`cdms_dev`), and make it as easy as possible to merge changes from the midas development team (`midas_dev`).

| branch   | purpose           |
| -------- | ----------------- |
| `master` | the stable branch |
| `cdms_dev` | the cdms development branch |
| `midas_dev` | in sync with the MIDAS development team |

If you wish to develop code, do so on `cdms_dev`.  Before beginning development make sure you're working with the latest Midas code: update `midas_dev` and merge any changes over to `cdms_dev`.  Once your changes are ready, create a patch and share it with the Midas team via an issue on Bitbucket.

(Note: you're welcome to instead share your code with Midas using a pull request; I recommend patches because pull requests are a pain to get right since we're working outside Bitbucket.)

The intent of this repository is to provide an area for testing code when discussing issues with the Midas team, and to allow for emergency commits when bugs impact data taking.  Ideally, we would only ever merge `midas_dev` onto our `master`.
